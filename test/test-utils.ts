import { DynamicModule } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'

export function testRepoModules(...entities): DynamicModule[] {
  const arr = [
      ConfigModule.forRoot({
        envFilePath: [`.env.${process.env.NODE_ENV}`, '.env'], 
        isGlobal: true,
        
      }),
      TypeOrmModule.forRoot({
        type:  process.env.DB_TYPE as any,
        database: process.env.DB_NAME,
        port: +process.env.DB_PORT,
        username: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        //entities: [__dirname + '/**/*.entity{.ts,.js}'],
        autoLoadEntities: true,
        // synchronize: true,
        logging: true,
        // debug: true,
        keepConnectionAlive: true,
    }),
    TypeOrmModule.forFeature([...entities])
  ]

  return arr
}