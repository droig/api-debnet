import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Cliente } from './entities/cliente.entity';
import { Empresa } from './entities/empresa.entity';

@Injectable()
export class ClienteService {

  constructor(
    @InjectRepository(Cliente)
    private clienteRepo: Repository<Cliente>,
    @InjectRepository(Empresa)
    private empresaRepo: Repository<Empresa>,
  ) {}


  buscaEmpresaCliente(rut: string) {
    return this.empresaRepo.findOne({where: {emprRut: rut.replace(/^0*/, '')}, relations: ['cliente']})
  }

}
