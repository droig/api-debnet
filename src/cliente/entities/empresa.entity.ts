import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Cliente } from './cliente.entity'

@Entity()
export class Empresa {

  @PrimaryGeneratedColumn()
  emprId: number

  @Column()
  emprRut: string

  @Column()
  emprRazonSocial: string

  @Column()
  emprDireccion: string

  @Column()
  emprComuna: string

  @Column()
  emprRegion: string

  @Column()
  emprEmail: string

  @Column()
  emprCiudad: string

  @Column()
  emprGiro: string

  @Column()
  emprFechaIn: string

  @Column()
  emprNumCuenta: string

  @Column()
  emprTelefono: string

  @Column()
  emprFechaPersoneria: Date

  @Column()
  emprNotaria: string

  @Column()
  emprNombreNotario: string

  @OneToOne(() => Cliente, cliente => cliente.empresa)
  cliente: Cliente
}