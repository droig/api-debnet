import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Empresa } from './empresa.entity'

@Entity()
export class Cliente {
  
  @PrimaryGeneratedColumn()
  clieId: number
  
  @Column()
  emprId: number

  @OneToOne(type => Empresa)
  @JoinColumn({name: 'emprId', referencedColumnName: 'emprId'})
  empresa: Empresa
  
  @Column()
  clieFechaIn: Date
  
  @Column()
  clieActivo: boolean
  
  @Column()
  usuaOnOf: number
  
  @Column()
  clieFirstTime: boolean
}
