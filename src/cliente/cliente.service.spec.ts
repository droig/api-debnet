import { Test, TestingModule } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import { getRepository, Repository } from 'typeorm';
import { testRepoModules } from '../../test/test-utils';
import { ClienteService } from './cliente.service';
import { Cliente } from './entities/cliente.entity';
import { Empresa } from './entities/empresa.entity';

describe('ClienteService', () => {
  let service: ClienteService
  let repoEmpresa: Repository<Empresa>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ ...testRepoModules(Empresa, Cliente) ],
      providers: [ClienteService],
    }).compile();

    service = module.get<ClienteService>(ClienteService)
    repoEmpresa = getRepository(Empresa)
  });

  it('join cliente empresa', async () => {
    // expect(service).toBeDefined();
    const repoCliente = getRepository(Cliente)
    const repoEmpresa = getRepository(Empresa)

    // este no funciona, no se pq
    const res2 = await repoCliente.find({
      relations: ['empresa'],
      // where: [{ empresa: { emprRut: '94297494' } } ]
      // where: [ { empresa: { emprId: 2100 } } ]
      where: [ { empresa: { emprTelefono: '56227355156' } } ]
    })

    // repoCliente.createQueryBuilder().leftJoinAndSelect

    // este si funciona
    // const res3 = await repoEmpresa.find({ where: { emprRut: '94297494', cliente: {clieId: 343} }, relations: ['cliente'] })
    console.log('res3')
  })
});
