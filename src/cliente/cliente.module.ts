import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ClienteService } from './cliente.service';
import { Cliente } from './entities/cliente.entity';
import { Empresa } from './entities/empresa.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Cliente, Empresa])],
  providers: [ClienteService],
  exports: [ClienteService]
})
export class ClienteModule {}
