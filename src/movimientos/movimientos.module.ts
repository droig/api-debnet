import { Module } from '@nestjs/common';
import { MovimientosService } from './movimientos.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Movimiento } from './entities/movimiento.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Movimiento])
  ],
  providers: [MovimientosService]
})
export class MovimientosModule {}
