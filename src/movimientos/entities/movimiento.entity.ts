import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Movimiento {
  
  @PrimaryGeneratedColumn() 
  moviId        : number

  @Column()
  clieId        : number
  
  @Column()
  moviAccion    : string
  
  @Column()
  moviNumeroDoc : string
  
  @Column()
  moviGlosa     : string
  
  @Column()
  moviCargo     : string
  
  @Column()
  moviAbono     : string
  
  @Column()
  moviSaldo     : string
  
  @Column()
  moviFechaIn   : Date
  
  @Column()
  moviEstado    : number
  
  @Column()
  padtId        : number
  
  @Column()
  writeId       : number
}
