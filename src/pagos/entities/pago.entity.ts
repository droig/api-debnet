import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Pago {
  
  @PrimaryGeneratedColumn()
  pagoId: number

  @Column({ nullable: true })
  pagoFechaIn: Date

  @Column({ nullable: true })
  clieId: number

  @Column({ nullable: true })
  pagoTipoDocumento: string

  @Column({ nullable: true })
  pagoNumeroDocumento: string

  @Column({ nullable: true })
  pagoFechaRecepcion: Date

  @Column({ nullable: true })
  pagoFechaEfectiva: Date

  @Column({ nullable: true })
  pagoMontoTotal: string

  @Column({ nullable: true })
  pagoTipoCobro: string

  @Column({ nullable: true })
  pagoFolioDocumento: string

  @Column({ nullable: true })
  pagoNumero: string

  @Column({ nullable: true })
  pagoUnico: string

  @Column({ nullable: true })
  pagoDescripcion: string
}
