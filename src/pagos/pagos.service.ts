import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'
import { CreatePagoDto } from './dto/create-pago.dto'
import { UpdatePagoDto } from './dto/update-pago.dto'
import { Pago } from './entities/pago.entity'

@Injectable()
export class PagosService {
  constructor(
    @InjectRepository(Pago)
    private repo: Repository<Pago>,
  ) {}

  create(createPagoDto: CreatePagoDto) {
    return 'This action adds a new pago'
  }

  findAll() {
    return this.repo.find({ take: 5 })
  }

  findOne(id: number) {
    return `This action returns a #${id} pago`
  }

  update(id: number, updatePagoDto: UpdatePagoDto) {
    return `This action updates a #${id} pago`
  }

  remove(id: number) {
    return `This action removes a #${id} pago`
  }
}
