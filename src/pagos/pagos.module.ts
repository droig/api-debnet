import { Module } from '@nestjs/common';
import { PagosService } from './pagos.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Pago } from './entities/pago.entity';
import { ClienteModule } from '../cliente/cliente.module';

@Module({
  imports: [TypeOrmModule.forFeature([Pago])],
  providers: [PagosService],
})
export class PagosModule {}
