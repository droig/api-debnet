import { Body, Controller, Post, UseGuards, Request, Get } from '@nestjs/common';
import { AuthService } from './auth/auth.service';
import { UserLoginDTO } from './auth/dto/user-login.dto';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { LocalAuthGuard } from './auth/local-auth.guard';
import { PagoDebnetDto } from './proceso-pago/dto/pago-debnet.dto';
import { ProcesoPagoService } from './proceso-pago/proceso-pago.service';

@Controller()
export class AppController {
  constructor(
    private authServ: AuthService,
    private readonly procesoPago: ProcesoPagoService
  ) {}

  @Post('pago-debnet')
  @UseGuards(JwtAuthGuard)
  async pagoDebnet(@Body() dto: PagoDebnetDto) {
    return await this.procesoPago.procesaPago(dto) ? { foo: 'bar' } : { foo: 'baz' }
  }

  @UseGuards(LocalAuthGuard)
  @Post('auth/login')
  login(@Request() req, @Body() user: UserLoginDTO) {
    return this.authServ.login(req.user)
  }
}
