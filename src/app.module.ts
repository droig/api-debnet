import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { MovimientosModule } from './movimientos/movimientos.module'
import { PagosModule } from './pagos/pagos.module'
import { ProcesoPagoModule } from './proceso-pago/proceso-pago.module'
import { ProcesoPagoService } from './proceso-pago/proceso-pago.service'
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [`.env.${process.env.NODE_ENV}`, '.env'],
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: (process.env.DB_TYPE as any) || 'sqlite',
      database: process.env.DB_NAME || 'db',
      port: +process.env.DB_PORT,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      host: process.env.DB_HOST || 'localhost',
      //entities: [__dirname + '/**/*.entity{.ts,.js}'],
      autoLoadEntities: true,
      // synchronize: true,
      // keepConnectionAlive: true
      // logging: true
    }),
    PagosModule,
    MovimientosModule,
    ProcesoPagoModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
