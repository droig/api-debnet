import { Test, TestingModule } from '@nestjs/testing';
import { testRepoModules } from '../test/test-utils';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProcesoPagoModule } from './proceso-pago/proceso-pago.module';

describe('AppController', () => {
  let appController: AppController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [...testRepoModules(), ProcesoPagoModule],
      controllers: [AppController],
      providers: [AppService],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('root', () => {
    it('`proceso pago - prueba 1`', async () => {
      // expect(appController.pagoDebnet()).toBe('Hello World!');
      const resultadoPago = await appController.pagoDebnet({
        holding: "400",
        fechaRecepcionDePago: "CI",
        correlativoDeCobro: "00001",
        codigoCliente: "1820",
        tipoCobro: "DD",
        numeroDocumento: "0007575996",
        fechaEfectivaPago: "",
        folioDocumento: "0007575996",
        descripcion: "Deposito DBC",
        numeroDeCobro: "17022021",
        pagoCodigoDocumento: "12",
        tipoDocumento: "40",
        rutDeudor: "00000000094297494",
        montoTotalPago: "0000000001313924",
        aplicaciones: [
          {
            folioDocumento: "0007575996",
            descripcion: "Deposito DBC",
            numeroDeCobro: "17022021",
            pagoCodigoDocumento: "12",
            tipoDocumento: "40",
            rutDeudor: "00000000094297494",
            montoTotalPago: "0000000001313924",
            fechaEfectivaPagodte: ""
          }
        ]
      });

      console.log(resultadoPago)
    });
  });
});
