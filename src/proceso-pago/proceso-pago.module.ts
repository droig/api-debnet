import { Module } from '@nestjs/common';
import { ClienteModule } from '../cliente/cliente.module';
import { ProcesoPagoService } from './proceso-pago.service';

@Module({
  imports: [ClienteModule],
  providers: [ProcesoPagoService],
  exports: [ProcesoPagoService]
})
export class ProcesoPagoModule {}
