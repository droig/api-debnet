import { IsNotEmpty } from 'class-validator'

export class Aplicacion {
  
  @IsNotEmpty()
  folioDocumento: string
  
  @IsNotEmpty()
  descripcion: string
  
  @IsNotEmpty()
  numeroDeCobro: string
  
  @IsNotEmpty()
  pagoCodigoDocumento: string
  
  @IsNotEmpty()
  tipoDocumento: string
  
  @IsNotEmpty()
  rutDeudor: string
  
  @IsNotEmpty()
  montoTotalPago: string
  
  @IsNotEmpty()
  fechaEfectivaPagodte: string
}