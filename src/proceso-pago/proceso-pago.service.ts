import { Injectable } from '@nestjs/common';
import { ClienteService } from '../cliente/cliente.service';
import { PagoDebnetDto } from './dto/pago-debnet.dto';

@Injectable()
export class ProcesoPagoService {

  constructor(
    private clienteServ: ClienteService
  ) {}

  async procesaPago(pago: PagoDebnetDto): Promise<boolean> {
    
    const clienteValido = await this.validaCiente(pago)
    // this.validaFactura(pago)
    // this.validaEstadosPago(pago)
    // this.validaDuplicado(pago)

    return clienteValido
  }

  private async validaCiente(pago: PagoDebnetDto) {
    const emp = await this.clienteServ.buscaEmpresaCliente(pago.rutDeudor)
    const clienteValido = !!emp && !!emp.cliente && emp.cliente.clieActivo
    if (!emp) throw new Error('No existe la empresa')
    if (emp.hasOwnProperty('cliente') && emp.cliente === undefined) throw new Error('Empresa con cliente indefinido')
    if (!emp.cliente) throw new Error('Empresa sin cliente')
    if (!emp.cliente.clieActivo) throw new Error('Cliente no activo')
    return clienteValido
  }

  private validaFactura(pago: PagoDebnetDto) {
    throw new Error('Method not implemented.')
  }

  private validaEstadosPago(pago: PagoDebnetDto) {
    throw new Error('Method not implemented.')
  }

  private validaDuplicado(pago: PagoDebnetDto) {
    throw new Error('Method not implemented.')
  }
}
