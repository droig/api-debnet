import { Test, TestingModule } from '@nestjs/testing';
import { getRepository, Repository } from 'typeorm';
import { testRepoModules } from '../../test/test-utils';
import { ClienteModule } from '../cliente/cliente.module';
import { Empresa } from '../cliente/entities/empresa.entity';
import { PagoDebnetDto } from './dto/pago-debnet.dto';
import { ProcesoPagoService } from './proceso-pago.service';

describe('ProcesoPagoService', () => {
  let service: ProcesoPagoService;
  let repoEmpresa: Repository<Empresa>

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [...testRepoModules(), ClienteModule],
      providers: [ProcesoPagoService],
    }).compile()

    service = module.get<ProcesoPagoService>(ProcesoPagoService)
    repoEmpresa = getRepository(Empresa)
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  
  it('validacion cliente - ok', async () => {
    jest.spyOn(repoEmpresa, 'findOne').mockResolvedValue({ emprId: 1, cliente: { clieActivo: true } } as Empresa)
    expect ( await service.procesaPago({ rutDeudor: '1111-1' } as PagoDebnetDto) ).toBeTruthy()
  })
    
  it('validacion cliente - cliente no activo', async () => {
    jest.spyOn(repoEmpresa, 'findOne').mockResolvedValue({ emprId: 1, cliente: { clieActivo: false } } as Empresa)
    await validaError(service, 'Cliente no activo')
  })

  it('validacion cliente - empresa sin relacion cliente', async () => {  
    jest.spyOn(repoEmpresa, 'findOne').mockResolvedValue({ emprId: 1, cliente: undefined } as Empresa)
    await validaError(service, 'Empresa con cliente indefinido')
  })

  it('validacion cliente - empresa sin cliente', async () => {  
    jest.spyOn(repoEmpresa, 'findOne').mockResolvedValue({ emprId: 1 } as Empresa)
    await validaError(service, 'Empresa sin cliente')
  })
    
  it('validacion cliente - no existe la empresa', async () => {  
    jest.spyOn(repoEmpresa, 'findOne').mockResolvedValue(undefined)
    await validaError(service, 'No existe la empresa')
  })
});


// funcion utilitaria

async function validaError(service, errorMsg) {
  try {
    const res = await service.procesaPago({ rutDeudor: '1111t-1' } as PagoDebnetDto)
  } catch (err) {
    expect(err).toHaveProperty('message', errorMsg)
  }
}
