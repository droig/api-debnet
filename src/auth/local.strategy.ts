import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super();
  }

  async validate(username: string, password: string): Promise<any> {
    const user = username === process.env.API_USER && password === process.env.API_PASSWORD
    if (!user) {
      throw new UnauthorizedException();
    }
    return {};
  }
}